#include "base/base.h"
#include "master/master.h"
#include "slave/slave.h"

#include <mpi.h>
#include <iostream>
#include <chrono>

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        std::cout << "Usage: " << argv[0] << " n_x_points world_size" << std::endl;
        return -1;
    }

    int global_n_x_points = std::stof(argv[1]);
    double dx = (double)(hes::x_max - hes::x_min) / global_n_x_points;
    int world_size_ = std::stoi(argv[2]);
    int n_slaves = world_size_ - 1;

    int provided_thread_level;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided_thread_level);
    if (provided_thread_level < MPI_THREAD_MULTIPLE) {
        std::cerr << "ERROR: MPI_THREAD_MULTIPLE not supported (the provided level of thread support is " << provided_thread_level << ")." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    // MPI_Init(NULL, NULL);
    int my_rank;
    int world_size;

    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    if (world_size != world_size_)
    {
        if (my_rank == hes::MASTER_PROCESS_RANK)
            std::cerr << "ERROR: World size is not equal to the specified one (" << world_size_ << ")." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    if (world_size < 2)
    {
        std::cerr << "ERROR: The minimum world size is 2 (current is " << world_size_ << ")." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
    }

    if (hes::MASTER_PROCESS_RANK >= world_size_)
    {
        if (my_rank == hes::MASTER_PROCESS_RANK)
            std::cerr << "ERROR: MASTER_PROCESS_RANK is not less than the world size. Check base/consts.h." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 2);
    }

    if (std::div(global_n_x_points, n_slaves).rem > 0)
    {
        if (my_rank == hes::MASTER_PROCESS_RANK)
            std::cerr << "ERROR: Cannot run with the specified arguments. n_x_points needs to be divisible by "
                         "(world_size - 1) for an even point partition."
                      << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 2);
    }

    hes::LoggerImpl logger(my_rank);

    try
    {
        if (my_rank == hes::MASTER_PROCESS_RANK)
        {
            hes::Master master(global_n_x_points, my_rank, n_slaves, &logger);
            master.run_master();
        }
        else
        {
            hes::Slave slave(global_n_x_points, my_rank, n_slaves, &logger);
            slave.run_slave();
        }
    }
    catch (std::exception &exc)
    {
        logger.log("ERROR: program failed with the following message: " + std::string(exc.what()));
        // logger.log_flush();
    }

    MPI_Finalize();
}

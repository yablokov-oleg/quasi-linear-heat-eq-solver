## Getting started
This is a program for computing a quasilinear heat equation on an MPI cluster.

The equation is defined as follows:

$`\begin{cases}
    \frac{\partial u}{\partial t} = \frac{\partial}{\partial x}\Big( k(u)\frac{\partial u}{\partial x} + r(u)u \Big) - u, & x \in [0, 1], & t > 0, & k(u) = 1 + u^2, & r(u) = sin^2(\pi u) \\
    u(x, 0) = u(0, t) = 0 \\
    u(1, t) = 1 - e^{-\omega t}, \quad \omega = 20
\end{cases}`$

You can change the initial conditions and `k(u)` and `r(u)` functions in `base/src/consts.h` and `base/src/math_base_functions/math_base_functions.cpp` files.

The computation is done using implicit finite difference method on a uniform grid, Newton's method and non-monotonic parallel tridiagonal matrix algorithm (this is all one pipeline).

## Build
The program uses MPI so that you nead a library for MPI support (MPICH, Open MPI, etc.).

This is an example of how to build the program:
```shell
cd quasi-linear-heat-eq-solver; mkdir build; cd build   # Make a build directory.
cmake ..                               		        # Generate a Makefile.
make                                                    # Build the program.
```

The binary file will appear in `build/<build_configuration>/bin`.

## Run
The way the program is run depends on the MPI library you use. For example, if you use Open MPI, have ten machines in your cluster and want to run the program on 999 points (1000 is not divisible by 9), you can type the following:
```
mpirun -np 10 ./solver 999 10
```
Here `n_x_points` is the number of x points the equation is computed in and `world_size` is the number of machines (or rather processes) in a cluster.

Note that to run the program on multiple machines you need a configured cluster, for example built on SSH and NFS. If you run the program on a single machine, you can still emulate a full-sized cluster, although all mpi processes will run on one machine (obviously).

## Debugging
Attaching a debugger depends on the MPI library, the debugger and the terminal you use. For example, if you use Open MPI, gdb, xterm and two processes (you can emulate a cluster on a single machine which is fine for debugging), you can type the following:
```
mpirun -np 2 xterm -e gdb --tui --args ./solver 10 2
```

## Documentation
The mathematical method implemented in this program is described in `doc/method.pdf` (in Russian).

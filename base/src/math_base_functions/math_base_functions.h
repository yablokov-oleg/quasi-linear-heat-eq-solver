#pragma once
#include "../structs.h"

#include <cmath>
#include <vector>

namespace hes
{

double k(double x); /** a math function */

double r(double x); /** a math function */

/**
 * @brief The main function which defines the heat equation problem.
 * The solution to the heat equation problem can be reformulated as finding x for which
 * f(x) = 0. Here x is represented by u_nxt. In other words, if u_cur is a known solution
 * for current time step and u_nxt is the solution for the next time step, then f(u_nxt) = 0.
 * In reality, f(x) is a vector so we demand f(x, i) = 0 for every i within range.
 * 
 * Don't use this function with i == 0 or i == u_cur.size() - 1, instead apply these 
 * boundary conditions yourself (because they can vary).
 */
double f(const std::vector<double> &u_cur, const std::vector<double> &u_nxt,
         const std::vector<double> &x,
         int i, int j, double dt, double t);

double get_left_boundary_u(double t); /**< left boundary condition for u */

double get_right_boundary_u(double t); /**< right boundary condition for u */

double u_init(double x); /**< the initial values of u */

/**
 * Defines a measure in the space of solutions.
 */
double compute_distance_between_solutions(const std::vector<double> &u1,
                                          const std::vector<double> &u2);

} // namespace hes

#pragma once

namespace hes
{

constexpr int MASTER_PROCESS_RANK = 0; /**< master's rank used by mpi; don't edit */

constexpr double dt = 0.01; /**< time step */

constexpr int n_time_steps = 50; /**< overall number of time steps to compute */

constexpr double x_min = 0.0; /**< min x for which solution is computed */

constexpr double x_max = 1.0; /**< max x for which solution is computed */

constexpr double w = 20.0; /**< a math coefficient */

constexpr double eps = 0.0001; /**< if the distance between two solutions (u1, u2) is 
                                    less than this, these solutions converge */

// here are commands used by master to tell slaves what to do:
constexpr int COMMAND__FINILIZE = 0;              /**< finish all work */
constexpr int COMMAND__GO_TO_NEXT_TIME_STEP = 1;  /**< compute solution for next time step */
constexpr int COMMAND__IMPROVE_APPROXIMATION = 2; /**< improve solution on current time step */
//

} // namespace hes

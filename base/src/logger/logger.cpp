#include "logger.h"
#include "../consts.h"

#include "../../../contrib/spdlog/include/spdlog/spdlog.h"
#include "contrib/spdlog/include/spdlog/sinks/daily_file_sink.h"

#include <iostream>
#include <exception>
#include <memory>

static std::shared_ptr<spdlog::logger> daily_logger;

namespace hes
{

LoggerImpl::LoggerImpl(int caller_rank) : caller_rank(caller_rank)
{
}

void LoggerImpl::log(const std::string& msg)
{
    static int log_rank;
    if (!daily_logger)
    {
        log_rank = caller_rank;
        std::string logger_name = (caller_rank == hes::MASTER_PROCESS_RANK) ? "master" : "slave" + std::to_string(caller_rank);
        daily_logger = spdlog::daily_logger_mt(logger_name, "log/" + logger_name + ".log");
    }
    else
        if (caller_rank != log_rank)
            throw(std::logic_error("hes::log called with different caller_rank"));
    
    daily_logger->critical(msg);
    daily_logger->flush(); // TODO: need to get rid of this, but without it logs are not flushed

    // NOTE: uncomment this when stopped debugging
    if (caller_rank == hes::MASTER_PROCESS_RANK)
    {
        std::cout << "master:\t" << msg << std::endl;
    }
    // else
    //     std::cout << "slave" << std::to_string(caller_rank) << ":\t" << msg << std::endl;
}

void log_flush()
{
    daily_logger->flush();
}

} // namespace hes

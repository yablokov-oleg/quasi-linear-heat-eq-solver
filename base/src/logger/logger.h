#pragma once
#include "../interfaces.h"

#include <string>

namespace hes
{

class LoggerImpl : public hes::Logger
{
public:
    LoggerImpl(int caller_rank);
    virtual ~LoggerImpl() {};

    virtual void log(const std::string &msg) override;

private:
    int caller_rank;
};

} // namespace hes

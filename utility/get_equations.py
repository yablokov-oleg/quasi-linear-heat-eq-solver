from sympy import Symbol, latex, sin, diff, expand, collect, pi, trigsimp

u_cur = Symbol('u^n_i')
u_cur_p1 = Symbol('u^n_{i+1}')
u_cur_m1 = Symbol('u^n_{i-1}')
u_nxt = Symbol('u^{n+1}_i')
u_nxt_p1 = Symbol('u^{n+1}_{i+1}')
u_nxt_m1 = Symbol('u^{n+1}_{i-1}')
dt = Symbol(r'\Delta t')
dx = Symbol(r'\Delta x')
k = 1 + pow(u_nxt, 2)
r = pow(sin(pi * u_nxt), 2)

f = (u_nxt - u_cur) / dt - diff(k, u_nxt) * (u_nxt_p1 - u_nxt) / dx - k * \
    (u_nxt_p1 - 2 * u_nxt + u_nxt_m1) / pow(dx, 2) - diff(r, u_nxt) * u_nxt - \
    r * (u_nxt_p1 - u_nxt) / dx + u_nxt

f_der = diff(f, u_nxt)
f_der_p1 = diff(f, u_nxt_p1)
f_der_m1 = diff(f, u_nxt_m1)

simplified_f_der = expand(f_der)
simplified_f_der_p1 = expand(f_der_p1)
simplified_f_der_m1 = expand(f_der_m1)

simplified_f_der = collect(simplified_f_der, u_nxt)
simplified_f_der_p1 = collect(simplified_f_der_p1, u_nxt_p1)
simplified_f_der_m1 = collect(simplified_f_der_m1, u_nxt_m1)

simplified_f_der = trigsimp(simplified_f_der)

print('f: ', latex(f))
print('\nsimplified f_der: ', latex(simplified_f_der))
print('\nsimplified f_der_p1: ', latex(simplified_f_der_p1))
print('\nsimplified f_der_m1: ', latex(simplified_f_der_m1))

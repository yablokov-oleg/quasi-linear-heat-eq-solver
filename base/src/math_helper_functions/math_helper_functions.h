#pragma once
#include "../structs.h"

#include <cmath>
#include <vector>
#include <utility>

namespace hes
{

/**
 * // n is the number of unknowns
 *
 * |b0 c0 0 ||x0| |d0|
 * |a1 b1 c1||x1|=|d1|
 * |0  a2 b2||x2| |d2|
 *
 * 1st iteration: b0x0 + c0x1 = d0 -> x0 + (c0/b0)x1 = d0/b0 ->
 *
 * x0 + g0x1 = r0               where g0 = c0/b0        , r0 = d0/b0
 *
 * 2nd iteration:     | a1x0 + b1x1   + c1x2 = d1
 * from 1st it.: -| a1x0 + a1g0x1        = a1r0
 * -----------------------------
 * (b1 - a1g0)x1 + c1x2 = d1 - a1r0
 *
 * x1 + g1x2 = r1               where g1=c1/(b1 - a1g0) , r1 = (d1 - a1r0)/(b1 - a1g0)
 *
 * 3rd iteration:      | a2x1 + b2x2   = d2
 * from 2nd it. : -| a2x1 + a2g1x2 = a2r2
 * -----------------------
 * (b2 - a2g1)x2 = d2 - a2r2
 * x2 = r2                      where                     r2 = (d2 - a2r2)/(b2 - a2g1)
 * Finally we have a triangular matrix:
 * |1  g0 0 ||x0| |r0|
 * |0  1  g1||x1|=|r1|
 * |0  0  1 ||x2| |r2|
 *
 * Condition: ||bi|| > ||ai|| + ||ci||
 *
 * Written by Keivan Moradi, 2014
 * Modified by Oleg Yablokov, 2019
 */
void solve_tridiagonal_equation(const std::vector<double> &a, const std::vector<double> &b,
                                const std::vector<double> &c, const std::vector<double> &d, 
                                std::vector<double> &x, int n);

double apply_boundary_conditions(std::vector<double> &u, double t); /**< boundary conditions for u */

} // namespace hes

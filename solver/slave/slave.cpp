#include "slave.h"

#include <mpi.h>
#include <functional>
#include <thread>

using hes::Slave;

Slave::Slave(int global_n_x_points, int rank, int n_slaves,
             hes::Logger *logger) : n_slaves(n_slaves),
                                    my_rank(rank), global_n_x_points(global_n_x_points),
                                    Logger(logger)
{
    local_n_x_points = global_n_x_points / n_slaves;
    last_received_command = hes::COMMAND__GO_TO_NEXT_TIME_STEP;

    u_cur.resize(local_n_x_points);
    u_nxt.resize(local_n_x_points);
    x.resize(local_n_x_points);
    y1.resize(local_n_x_points);
    y2.resize(local_n_x_points);
    y3.resize(local_n_x_points);
    a.resize(local_n_x_points);
    b.resize(local_n_x_points);
    c.resize(local_n_x_points);
    d1.resize(local_n_x_points);
    d2.resize(local_n_x_points);
    d3.resize(local_n_x_points);

    double dx = (hes::x_max - hes::x_min) / (global_n_x_points - 1);
    x[0] = hes::x_min + dx * (my_rank - 1) * local_n_x_points;
    for (int i = 1; i < local_n_x_points; ++i)
        x[i] = x[i - 1] + dx;
}

Slave::~Slave()
{
}

void Slave::run_slave()
{
    Logger->log("slave is running...");
    while (last_received_command != hes::COMMAND__FINILIZE)
    {
        receive_u();
        compute_basis_y();
        send_basis_y();
        receive_next_command();
    }
}

void Slave::receive_u()
{
    MPI_Status status;
    if (last_received_command == hes::COMMAND__GO_TO_NEXT_TIME_STEP)
    {
        MPI_Recv(&u_cur[0], local_n_x_points, MPI_DOUBLE, hes::MASTER_PROCESS_RANK, 0, MPI_COMM_WORLD, &status);
        u_nxt = u_cur;
    }
    else if (last_received_command == hes::COMMAND__IMPROVE_APPROXIMATION)
        MPI_Recv(&u_nxt[0], local_n_x_points, MPI_DOUBLE, hes::MASTER_PROCESS_RANK, 0, MPI_COMM_WORLD, &status);
    else
        throw("reached receive_u() but last_received_command == hes::COMMAND__FINILIZE");

    Logger->log("received u");
}

void Slave::compute_basis_y()
{
    double t = hes::dt * nxt_time_step;

    // update 3-diag matrix
    a[0] = 0;
    b[0] = 1;
    c[0] = 0;
    for (int i = 1; i < local_n_x_points - 1; ++i)
    {
        a[i] = hes::f(u_cur, u_nxt, x, i, i - 1, hes::dt, t);
        b[i] = hes::f(u_cur, u_nxt, x, i, i, hes::dt, t);
        c[i] = hes::f(u_cur, u_nxt, x, i, i + 1, hes::dt, t);
    }
    a.back() = 0;
    b.back() = 1;
    c.back() = 0;

    // compute y1
    d1[0] = 0;
    for (int i = 1; i < local_n_x_points - 1; ++i)
        d1[i] = -u_cur[i] / dt;
    d1.back() = 0;
    std::thread thr1([&]() { hes::solve_tridiagonal_equation(a, b, c, d1, y1, local_n_x_points); });

    // compute y2
    std::fill(d2.begin(), d2.end(), 0);
    d2.back() = 1;
    std::thread thr2([&]() { hes::solve_tridiagonal_equation(a, b, c, d2, y2, local_n_x_points); });

    // compute y3
    std::fill(d3.begin(), d3.end(), 0);
    d3[0] = 1;
    std::thread thr3([&]() { hes::solve_tridiagonal_equation(a, b, c, d3, y3, local_n_x_points); });

    thr1.join();
    thr2.join();
    thr3.join();
    Logger->log("computed basis functions");
}

void Slave::send_basis_y()
{
    MPI_Send(&y1[0], local_n_x_points, MPI_DOUBLE, hes::MASTER_PROCESS_RANK, 1, MPI_COMM_WORLD);
    MPI_Send(&y2[0], local_n_x_points, MPI_DOUBLE, hes::MASTER_PROCESS_RANK, 2, MPI_COMM_WORLD);
    MPI_Send(&y3[0], local_n_x_points, MPI_DOUBLE, hes::MASTER_PROCESS_RANK, 3, MPI_COMM_WORLD);
    Logger->log("sent basis functions");
}

void Slave::receive_next_command()
{
    MPI_Bcast(&last_received_command, 1, MPI_INT, hes::MASTER_PROCESS_RANK, MPI_COMM_WORLD);
    if (last_received_command == hes::COMMAND__GO_TO_NEXT_TIME_STEP)
        ++nxt_time_step;
    Logger->log("received next command");
}

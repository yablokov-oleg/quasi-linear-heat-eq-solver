#include "math_helper_functions.h"
#include "../math_base_functions/math_base_functions.h"
#include "../consts.h"

#include <cmath>
#include <stdexcept>

namespace hes
{

void solve_tridiagonal_equation(const std::vector<double> &a, const std::vector<double> &b,
                                const std::vector<double> &c, const std::vector<double> &d,
                                std::vector<double> &x, int n)
{
    if (n == 0) return;
    std::vector<double> c_ = c;
    x = d;

    n--; // since we start from x0 (not x1)
    c_[0] /= b[0];
    x[0] /= b[0];

    for (int i = 1; i < n; i++)
    {
        c_[i] /= b[i] - a[i] * c_[i - 1];
        x[i] = (x[i] - a[i] * x[i - 1]) / (b[i] - a[i] * c_[i - 1]);
    }

    x[n] = (x[n] - a[n] * x[n - 1]) / (b[n] - a[n] * c_[n - 1]);

    for (int i = n; i-- > 0;)
    {
        x[i] -= c_[i] * x[i + 1];
    }
}

double apply_boundary_conditions(std::vector<double> &u, double t)
{
    u.front() = hes::get_left_boundary_u(t);
    u.back() = hes::get_right_boundary_u(t);
}

} // namespace hes

#pragma once
#include <string>

namespace hes
{

struct Logger
{
    virtual void log(const std::string& msg) = 0;

    virtual ~Logger() {};
};

} // namespace hes

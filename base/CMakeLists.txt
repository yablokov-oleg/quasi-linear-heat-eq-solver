cmake_minimum_required(VERSION 3.5)
project(base)

add_library(base 
    src/math_base_functions/math_base_functions.cpp
    src/math_helper_functions/math_helper_functions.cpp
    src/logger/logger.cpp)

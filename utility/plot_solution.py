import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import click


fig, ax = plt.subplots()


def animate(i, *fargs):
    data = fargs[0]
    line = fargs[1]
    line.set_ydata(data[(i + 1) % len(data)])
    return line,


@click.command()
@click.option('--solution', default='solution.txt', help='Solution filepath')
@click.option('--interval', default=50, help='Inteval between frames')
def plot_solution(solution, interval):
    data = []

    print('Reading {solution}...'.format(**locals()))
    with open(solution, 'r') as f:
        for line in f:
            data.append(np.array(line.split(' ')[:-1], dtype=np.float64))
    data = np.array(data)

    global_n_x_points = data.shape[1]
    x_min = 0
    x_max = 1
    dx = (x_max - x_min) / global_n_x_points
    ax.set_ylim(bottom=0, top=np.amax(data))

    x = np.arange(x_min, x_max, dx)
    line, = ax.plot(x, data[0])
    ani = animation.FuncAnimation(fig, animate, fargs=(data, line),
                                  interval=interval, blit=False)
    plt.show()


if __name__ == '__main__':
    plot_solution()

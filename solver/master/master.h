#pragma once

#include "base/base.h"
#include <vector>

namespace hes
{

/**
 * @brief A class which implements master processes' logic.
 * The following logic is performed when run_master() is called:
 * 1) apply initial conditions;
 * 2) write data to disk;
 * 3) send u to slaves;
 * 4) update main equation;
 * 5) receive basis functions (y) from slaves;
 * 6) compute boundary y;
 * 7) compute u_nxt;
 * 8) choose next command (improve solution, go to next time step or finilize);
 * 9) send next command to slaves;
 * 10) repeat 3)-9) until finilized.
 */
class Master
{
public:
    Master(int global_n_x_points, int rank, int n_slaves, hes::Logger *logger);
    ~Master();

    void run_master();

private:
    void apply_initial_conditions();
    void send_u();
    void update_main_equation();
    void receive_basis_y();
    void compute_boundary_y();
    void compute_u_nxt();
    void choose_next_command();
    void send_next_command();

    void write_data_to_disk();

    std::vector<double> u_cur;           /**< solution for current time step */
    std::vector<double> u_nxt;           /**< solution for next time step */
    std::vector<double> u_nxt_prev_iter; /**< previous (less accurate) solution for next time step */
    std::vector<double> x;               /**< x values */

    std::vector<double> a; /**< lower diagonal elements in 3-diag matrix in main equation */
    std::vector<double> b; /**< central diagonal elements in 3-diag matrix in main equation */
    std::vector<double> c; /**< lower diagonal elements in 3-diag matrix in main equation */
    std::vector<double> d; /**< right part in main equation */

    std::vector<double> y1; /**< basis y (1) */
    std::vector<double> y2; /**< basis y (2) */
    std::vector<double> y3; /**< basis y (3) */

    std::vector<double> a_r;        /**< lower diagonal elements in 3-diag matrix in boundary_y equation */
    std::vector<double> b_r;        /**< central diagonal elements in 3-diag matrix in boundary_y equation */
    std::vector<double> c_r;        /**< lower diagonal elements in 3-diag matrix in boundary_y equation */
    std::vector<double> d_r;        /**< right part of boundary_y equation */
    std::vector<double> boundary_y; /**< solution to boundary_y equation */

    std::vector<int> slave_start_pos; /**< i-th slave start in slave_start_pos[i]-th x point */

    int n_slaves;                   /**< overall number of slaves in cluster */
    int global_n_x_points;          /**< overall number of x points */
    int local_n_x_points;           /**< number of x points assigned to one slave */
    int cur_command;                /**< next command for slaves to be done */
    int nxt_time_step = 1;          /**< next time step */
    int cur_approximation_step = 0; /**< number of times current solution was improved */

    hes::Logger *Logger; /**< a logger */
};

} // namespace hes

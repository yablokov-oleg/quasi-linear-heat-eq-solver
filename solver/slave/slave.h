#pragma once

#include "base/base.h"
#include <vector>

namespace hes
{

/**
 * @brief A class which implements slave processes' logic.
 * The following logic is performed when run_slave() is called:
 * 1) receive u from master;
 * 2) compute basis functions (y);
 * 3) send basis functions (y);
 * 4) receive next command from master (improve solution, go to next time step or finilize);
 * 5) repeat 1)-4) until finilized.
 */
class Slave
{
public:
    Slave(int global_n_x_points, int rank, int n_slaves, hes::Logger *logger);
    ~Slave();

    void run_slave();

private:
    void receive_u();
    void compute_basis_y();
    void send_basis_y();
    void receive_next_command();

    std::vector<double> u_cur; /**< solution for current time step */
    std::vector<double> u_nxt; /**< solution for next time step */
    std::vector<double> y1;    /**< basis y (1); includes boundary conditions */
    std::vector<double> y3;    /**< basis y (2); includes boundary conditions */
    std::vector<double> y2;    /**< basis y (3); includes boundary conditions */

    std::vector<double> a; /**< used for 3-diag matrix; includes boundary conditions */
    std::vector<double> b; /**< used for 3-diag matrix; includes boundary conditions */
    std::vector<double> c; /**< used for 3-diag matrix; includes boundary conditions */

    std::vector<double> d1;
    std::vector<double> d2;
    std::vector<double> d3;

    std::vector<double> x;     /**< x values */
    int n_slaves;              /**< overall number of slaves in cluster */
    int global_n_x_points;     /**< overall number of x points */
    int local_n_x_points;      /**< number of x points assigned to this slave */
    int nxt_time_step = 1;     /**< next time step */
    int last_received_command; /**< last command from master to be done */
    int my_rank;               /**< this slave's rank */

    hes::Logger *Logger; /**< a logger */
};

} // namespace hes

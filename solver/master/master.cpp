#include "master.h"

#include <mpi.h>
#include <functional>
#include <algorithm>
#include <functional>
#include <fstream>
#include <thread>
#include <memory>

using hes::Master;

Master::Master(int global_n_x_points, int rank, int n_slaves,
               hes::Logger *logger) : n_slaves(n_slaves),
                                      global_n_x_points(global_n_x_points),
                                      Logger(logger)
{
    local_n_x_points = global_n_x_points / n_slaves;
    cur_command = hes::COMMAND__GO_TO_NEXT_TIME_STEP;

    u_cur.resize(global_n_x_points);
    u_nxt.resize(global_n_x_points);
    u_nxt_prev_iter.resize(global_n_x_points);
    x.resize(global_n_x_points);
    y1.resize(global_n_x_points);
    y2.resize(global_n_x_points);
    y3.resize(global_n_x_points);
    a.resize(global_n_x_points);
    b.resize(global_n_x_points);
    c.resize(global_n_x_points);
    d.resize(global_n_x_points);
    a_r.resize(2 * n_slaves - 2);
    b_r.resize(2 * n_slaves - 2);
    c_r.resize(2 * n_slaves - 2);
    d_r.resize(2 * n_slaves - 2);
    boundary_y.resize(2 * n_slaves - 2);

    double dx = (hes::x_max - hes::x_min) / (global_n_x_points - 1);
    for (int i = 0; i < global_n_x_points; ++i)
        x[i] = hes::x_min + i * dx;

    slave_start_pos.resize(n_slaves);
    for (int i = 0; i < n_slaves; ++i)
        slave_start_pos[i] = i * local_n_x_points;

    remove("solution.txt");
}

Master::~Master()
{
    Logger->log("master is finilized");
}

void Master::run_master()
{
    Logger->log("master is running...");
    apply_initial_conditions();
    write_data_to_disk();
    while (cur_command != hes::COMMAND__FINILIZE)
    {
        send_u();
        update_main_equation();
        receive_basis_y();
        compute_boundary_y();
        compute_u_nxt();
        choose_next_command();
        send_next_command();
    }
}

void Master::apply_initial_conditions()
{
    for (int i = 0; i < global_n_x_points; ++i)
        u_cur[i] = hes::u_init(i);
    hes::apply_boundary_conditions(u_cur, hes::dt * (nxt_time_step - 1));

    u_nxt = u_cur;
    u_nxt_prev_iter = u_nxt;

    Logger->log("applied initial conditions");
}

void Master::send_u()
{
    std::vector<std::unique_ptr<std::thread>> threads(n_slaves);
    for (int i = 0; i < n_slaves; ++i)
        threads[i].reset(new std::thread([=](int i) {
            MPI_Send(&u_nxt[slave_start_pos[i]], local_n_x_points, MPI_DOUBLE, i + 1, 0, MPI_COMM_WORLD);
        },
                                         i));
    for (int i = 0; i < n_slaves; ++i)
        threads[i]->join();

    Logger->log("sent u (" + std::to_string(nxt_time_step + 1) + "/" + std::to_string(n_time_steps) +
                " time step)"); // + 1 because we want to show count from 1 and 1 is already computed as initial conditions
}

void Master::update_main_equation()
{
    double t = hes::dt * nxt_time_step;

    a[0] = 0;
    b[0] = 1;
    c[0] = 0;
    d[0] = get_left_boundary_u(t);
    for (int i = 1; i < global_n_x_points - 1; ++i)
    {
        a[i] = hes::f(u_cur, u_nxt, x, i, i - 1, hes::dt, t);
        b[i] = hes::f(u_cur, u_nxt, x, i, i, hes::dt, t);
        c[i] = hes::f(u_cur, u_nxt, x, i, i + 1, hes::dt, t);
        d[i] = -u_cur[i] / dt;
    }
    a.back() = 0;
    b.back() = 1;
    c.back() = 0;
    d.back() = get_right_boundary_u(t);

    Logger->log("equation updated");
}

void Master::receive_basis_y()
{
    MPI_Status status;
    std::vector<double> buff(local_n_x_points);
    for (int i = 0; i < n_slaves * 3; ++i)
    {
        MPI_Recv(&buff[0], local_n_x_points, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if (status.MPI_TAG == 1)
            std::copy(buff.cbegin(), buff.cend(), y1.begin() + slave_start_pos[status.MPI_SOURCE - 1]);
        else if (status.MPI_TAG == 2)
            std::copy(buff.cbegin(), buff.cend(), y2.begin() + slave_start_pos[status.MPI_SOURCE - 1]);
        else if (status.MPI_TAG == 3)
            std::copy(buff.cbegin(), buff.cend(), y3.begin() + slave_start_pos[status.MPI_SOURCE - 1]);
        else
            throw(std::runtime_error("received wrong mpi tag from slave"));
    }

    Logger->log("received basis y");
}

void Master::compute_boundary_y()
{
    double t = nxt_time_step * hes::dt;
    int i1, i2;
    for (int i = 0; i < 2 * n_slaves - 2; i += 2)
    {
        i1 = (i / 2 + 1) * local_n_x_points;
        i2 = i1 - 1;
        a_r[i] = a[i2] * y3[i2 - 1];
        b_r[i] = b[i2] + a[i2] * y2[i2 - 1];
        c_r[i] = c[i2];
        d_r[i] = d[i2] - a[i2] * y1[i2 - 1];

        a_r[i + 1] = a[i1];
        b_r[i + 1] = b[i1] + c[i1] * y3[i1 + 1];
        c_r[i + 1] = c[i1] * y2[i1 + 1];
        d_r[i + 1] = d[i1] - c[i1] * y1[i1 + 1];
    }
    if (n_slaves > 1)
    {
        d_r[0] -= a_r[0] * hes::get_left_boundary_u(t);
        a_r[0] = 0;
        d_r.back() -= c_r.back() * hes::get_right_boundary_u(t);
        c_r.back() = 0;
    }

    hes::solve_tridiagonal_equation(a_r, b_r, c_r, d_r, boundary_y, 2 * n_slaves - 2);

    Logger->log("computed boundary_y");
}

void Master::compute_u_nxt()
{
    double t = nxt_time_step * hes::dt;
    double boundary_y_l, boundary_y_r;
    for (int i = 0; i < n_slaves; ++i)
    {
        int k = slave_start_pos[i];
        boundary_y_l = i == 0 ? hes::get_left_boundary_u(t) : boundary_y[2 * i - 1];
        boundary_y_r = i == n_slaves - 1 ? hes::get_right_boundary_u(t) : boundary_y[2 * i];
        for (int j = 0; j < local_n_x_points; ++j)
            u_nxt[k + j] = y1[k + j] + boundary_y_l * y3[k + j] + boundary_y_r * y2[k + j];
    }

    cur_approximation_step += 1;
    Logger->log("computed u");
}

void Master::choose_next_command()
{
    if (cur_command == hes::COMMAND__GO_TO_NEXT_TIME_STEP)
    { // only first iteration passed -> unconditional improve approximation
        Logger->log("next command is COMMAND__IMPROVE_APPROXIMATION (step = " + std::to_string(cur_approximation_step) + ")");
        cur_command = hes::COMMAND__IMPROVE_APPROXIMATION;
    }
    else
    {
        double distance = hes::compute_distance_between_solutions(u_nxt, u_nxt_prev_iter);
        if (distance <= hes::eps)
        {
            write_data_to_disk();

            ++nxt_time_step;
            u_cur = u_nxt;

            if (nxt_time_step >= hes::n_time_steps)
            {
                Logger->log("next command is COMMAND__FINILIZE (distance = " + std::to_string(distance) + ")");
                cur_command = hes::COMMAND__FINILIZE;
            }
            else
            {
                Logger->log("next command is COMMAND__GO_TO_NEXT_TIME_STEP (distance = " + std::to_string(distance) + ")");
                cur_command = hes::COMMAND__GO_TO_NEXT_TIME_STEP;
                cur_approximation_step = 0;
            }
        }
        else
        {
            Logger->log("next command is COMMAND__IMPROVE_APPROXIMATION (step = " + std::to_string(cur_approximation_step) +
                        "; distance = " + std::to_string(distance) + ")");
        }
    }
    u_nxt_prev_iter = u_nxt;
}

void Master::send_next_command()
{
    MPI_Bcast(&cur_command, 1, MPI_INT, hes::MASTER_PROCESS_RANK, MPI_COMM_WORLD);
    Logger->log("sent next command");
}

void Master::write_data_to_disk()
{
    std::ofstream file("solution.txt", std::ofstream::app);
    std::string u_nxt_;
    for (auto x : u_nxt)
        u_nxt_ += std::to_string(x) + " ";
    file << u_nxt_ << std::endl;
    file.close();
}

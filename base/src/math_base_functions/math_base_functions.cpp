#include "math_base_functions.h"
#include "../consts.h"

#include <cmath>
#include <stdexcept>

#include <iostream>

namespace hes
{

double k(double u)
{
    return 1.0 + u * u;
}

double r(double u)
{
    return std::pow(std::sin(u * M_PI), 2);
}

/**
 * \[ e\_ratio = \frac{e_i}{e_{i - 1}} \approx exp \{\frac{r(y_i)h_i}{k(y_i)} \} \]
 * where
 * \[ e_i = exp \{\int_{0}^{x_i} \frac{r(y(x))}{k(y(x))} dx \}
 * \approx exp \{\sum_{j=0}^{i} \frac{r(y_j)}{k(y_j)} h_j \} \]
 */
double e_ratio(const std::vector<double> &u,
               const std::vector<double> &x,
               int i)
{
    auto _h = [=](int i) { return x[i] - x[i - 1]; };
    auto _h_avg = [=](int i) { return (_h(i) + _h(i + 1)) / 2.0; };
    return std::exp(r(u[i]) * _h_avg(i) / k(u[i]));
}

/**
 * \[ k\_tilda = \left(\int_{x_i}^{x_{i + 1}} \frac{dx}{k(y(x))} \right)^{-1} 
 * \approx \left(\frac{h_i}{k(y_i)} + \frac{h_{i + 1}}{k(y_{i + 1})} \right) ^{-1} \]
 */
double k_tilda(const std::vector<double> &u,
               const std::vector<double> &x,
               int i)
{
    auto _h = [=](int i) { return x[i] - x[i - 1]; };
    return 1.0 / (_h(i + 1) / k(u[i + 1]) + _h(i) / k(u[i]));
}

double f(const std::vector<double> &u_cur, const std::vector<double> &u_nxt,
         const std::vector<double> &x, int i, int j, double dt, double t)
{
    if ((i < 1) || (i > (u_cur.size() - 2)))
        throw(std::invalid_argument("i is not within range"));
    if ((j < 0) || (j > (u_cur.size() - 1)))
        throw(std::invalid_argument("j is not within range"));

    auto _h = [=](int i) { return x[i] - x[i - 1]; };
    auto _h_avg = [=](int i) { return (_h(i) + _h(i + 1)) / 2.0; };
    auto _e_ratio = [=](int i) { return e_ratio(u_nxt, x, i); };
    auto _k_tilda = [=](int i) { return k_tilda(u_nxt, x, i); };

    if (j == i)
        return -2.0 * (_k_tilda(i) / ((_e_ratio(i + 1) + 1.0) * _h(i + 1)) + _k_tilda(i - 1) * _e_ratio(i) / ((_e_ratio(i) + 1.0) * _h(i))) / _h_avg(i) 
            - 1.0 - 1.0 / dt;
    else if (j == i + 1)
        return 2.0 * _k_tilda(i) * _e_ratio(i + 1) / ((_e_ratio(i + 1) + 1.0) * _h(i + 1) * _h_avg(i));
    else if (j == i - 1)
        return 2.0 * _k_tilda(i - 1) / ((_e_ratio(i) + 1.0) * _h(i) * _h_avg(i));
    else
        return 0;
}

double get_left_boundary_u(double t)
{
    return 0;
}

double get_right_boundary_u(double t)
{
    return 1.0 - std::exp(-hes::w * t);
}

double u_init(double x)
{
    return 0;
}

double compute_distance_between_solutions(const std::vector<double> &u1, const std::vector<double> &u2)
{
    if (u1.size() != u2.size())
        throw(std::invalid_argument("u1.size() != u2.size()"));

    double max_dist = 0;
    for (int i = 0; i < u1.size(); ++i)
        max_dist = std::max(max_dist, std::abs(u1[i] - u2[i]));
    return max_dist;
}

} // namespace hes
